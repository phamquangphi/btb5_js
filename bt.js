//BÀI TẬP MỘT: QUẢN LÝ TUYỂN SINH
function chonKhuVuc(){
      if(document.getElementById("khu_vuc").value == "x"){
        return 0;
      } else if(document.getElementById("khu_vuc").value == "a"){
        return 2;
      }else if(document.getElementById("khu_vuc").value == "b"){
        return 1;
      }else if(document.getElementById("khu_vuc").value == "c"){
        return 0.5;
      }
}
function chonDoiTuong(){
  if(document.getElementById("doi_tuong").value == "0"){
    return 0;
  } else if(document.getElementById("doi_tuong").value == "1"){
    return 2.5;
  } else if(document.getElementById("doi_tuong").value == "2"){
    return 1.5;
  } else if(document.getElementById("doi_tuong").value == "3"){
    return 1;
  }
}
function tinh_tong(){
    var monMot = document.getElementById('mon_mot').value*1;
    var monHai = document.getElementById('mon_hai').value*1;
    var monBa =document.getElementById('mon_ba').value*1;
    var diemChuan = document.getElementById('so_tong').value;
    var khuVuc = chonKhuVuc();
    var doiTuong = chonDoiTuong();


    var diemTB = (monMot + monHai +monBa + khuVuc + doiTuong);
    if(diemTB >= diemChuan && monMot > 0 && monHai > 0 && monBa > 0){
         return document.getElementById("suat").innerHTML = ` ${diemTB} - thí sinh đậu `
    } if(diemTB < diemChuan && monMot > 0 && monHai > 0 && monBa > 0){
      return document.getElementById("suat").innerHTML = `${diemTB} - Bạn không đủ điểm`
    } else{
     return document.getElementById("suat").innerHTML = `bạn có 1 trong 3 môn là điểm 0 `;
    }

}
//BÀI TẬP HAI: TÍNH TIỀN ĐIỆN
const kw50Dau = 500;
const kw50Ke = 650;
const kw100 = 850;
const kw150 = 1100;
const kwConlai = 1300;
function tinhtien(){
  var nhapTen = document.getElementById("nhapTen").value;
  var soKW = document.getElementById("soKW").value*1;
  var tinhtien = 0;
   if(soKW <= 50){
     tinhtien = soKW * kw50Dau;
   } else if(soKW <= 100){
    tinhtien = (50 * kw50Dau) + (soKW - 50)* kw50Ke;
   }else if(soKW <= 200){
    tinhtien= (50* kw50Dau) + ( 50 * kw50Ke) + (soKW - 100)*kw100;
   } else if(soKW <= 350){
    tinhtien = (50*kw50Dau) + (50*kw50Ke) + (100*kw100) + (soKW - 200)*kw150;
   } else {
    tinhtien = ( 50*kw50Dau) + (50*kw50Ke) + (100*kw100) + (soKW - 350)*kwConlai;
   }
  return document.getElementById("ketqua").innerHTML= `${nhapTen} - ${tinhtien}`;
    
  }
//BÀI TẬP BA: TÍNH THUẾ THU NHẬP CÁ NHÂN

function thunhap(){
  var hoTen = document.getElementById("nhap_ten").value;
  var thuNhap = document.getElementById("thu_nhap").value*1;
  var phuThuoc = document.getElementById("phu_thuoc").value*1;
  var thuNhapChiuthue = thuNhap - 4000000 - phuThuoc * 1600000;
  var tienThue = 0;
  if(thuNhap == "" || thuNhap*1 <0){
    return document.getElementById("so_tien").innerHTML= `${hoTen}-số tiền bạn nhập không hợp lệ, mời bạn nhập lại`
  } else{
  if (thuNhapChiuthue <= 60e+6) {
    tienThue = thuNhapChiuthue * 0.05;
  } else if (thuNhapChiuthue > 60e+6 && thuNhapChiuthue <=120e+6) {
    tienThue = (60e+6 * 0.05) + (thuNhapChiuthue - 60e+6)*0.1;
  } else if (thuNhapChiuthue > 120e+6 && thuNhapChiuthue <= 210e+6) {
    tienThue = (60e+6*0.05) + (120e+6*0.1) + (thuNhapChiuthue - 120e+6)*0.15;
  } else if(thuNhapChiuthue > 210e+6 && thuNhapChiuthue <=384e+6){
    tienThue = (60e+6*0.05) + (120e+6*0.1) + (210e+6*0.15) + (thuNhapChiuthue - 210e+6)*0.2;
  } else if (thuNhapChiuthue > 384e+6 && thuNhapChiuthue <= 624e+6) {
    tienThue = (60e+6*0.05) + (120e+6*0.1) + (210e+6*0.15) + (384e+6*0.2) + (thuNhapChiuthue - 384e+6)*0.25;
  }else if (thuNhapChiuthue > 624e+6 && thuNhapChiuthue <= 960e+6) {
    tienThue = (60e+6*0.05) + (120e+6*0.1) + (210e+6*0.15) + (384e+6*0.2) + (624e+6*0.25) + (thuNhapChiuthue - 624e+6)*0.3;
  } else {
    tienThue = (60e+6*0.05) + (120e+6*0.1) + (210e+6*0.15) + (384e+6*0.2) + (624e+6*0.25) + (960e+6*0.3) + (thuNhapChiuthue - 960e+6)*0.35;
  }
  return document.getElementById("so_tien").innerHTML=`${hoTen} - ${tienThue} VND`;
  }
}
//BÀI TẬP 4: 
document.querySelector("#soKetnoi").style.display ="none";
document.querySelector("#chon_doituong").onchange = function() {
  var chon = document.querySelector("#chon_doituong").value;
  if (chon == "1") {
    document.querySelector("#soKetnoi").style.display = "block";
  } else {
    document.querySelector("#soKetnoi").style.display = "none";
  }
};
function bill(){
  var khachHang = document.getElementById("chon_doituong").value;
  var soKetnoi = document.getElementById("number_test").vlue*1;
  var maKhachhang = document.getElementById("ma_khach_hang").value;
  var soKenh = document.getElementById("so_kenh").value*1;
  var tienCap = 0;
  if(chon == 0){
    alert("vui lòng khách hàng chọn lại");
    return;
  } else if(chon == "2"){
    tienCap = 4.5 + 20.5 + 7.5*soKenh;
  } else if(chon == "1"){
    var tienKetnoi =0;
    if (soKetnoi >10) {
      tienKetnoi = 75 + (soKetnoi - 10)*5;
    } else{
      tienKetnoi = 75;
    }
    tienCap = 15 + tienKetnoi + 50*soKenh;
  }
  return document.getElementById("result").innerHTML = `${khachHang} - ${maKhachhang} - ${tienCap} $`
}

